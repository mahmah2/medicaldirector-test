using MedicalDirectorBusiness;
using System;
using Xunit;
using System.Diagnostics;
using Xunit.Abstractions;

namespace MedicalDirectorTests
{
    public class UnitTest1
    {
        private readonly ITestOutputHelper output;

        public UnitTest1(ITestOutputHelper output)
        {
            this.output = output;
        }


        [Fact]
        public void TestLogic1()
        {
            var logic = new PatientGroups();

            var input = new int[6, 6]
            {
                {1, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 1},
                {1, 1, 0, 1, 0, 0}
            };

            Assert.Equal(4, logic.Calculate(input));
        }

        [Fact]
        public void TestLogic2()
        {
            var logic = new PatientGroups();

            var input = new int[6, 5]
            {
                {1, 0, 1, 1, 1},
                {1, 0, 0, 0, 0},
                {1, 0, 0, 0, 1},
                {0, 0, 1, 0, 0},
                {0, 1, 0, 0, 0},
                {0, 1, 0, 0, 1}
            };

            Assert.Equal(5, logic.Calculate(input));
        }

        [Fact]
        public void TestBoundary0()
        {
            var logic = new PatientGroups();

            Assert.Throws<ArgumentNullException>(() => logic.Calculate(null));
        }

        [Fact]
        public void TestBoundary1()
        {
            var logic = new PatientGroups();

            var input = new int[0, 0];

            Assert.Equal(0, logic.Calculate(input));
        }

        [Fact]
        public void TestBoundary2()
        {
            var logic = new PatientGroups();

            var input = new int[1, 1]
            {
                {1},
            };

            Assert.Equal(1, logic.Calculate(input));
        }

        [Fact]
        public void TestBoundary3()
        {
            var logic = new PatientGroups();

            var input = new int[2, 2]
            {
                {1,1},
                {1,1},
            };

            Assert.Equal(1, logic.Calculate(input));
        }

        [Fact]
        public void TestBoundary4()
        {
            var logic = new PatientGroups();

            var input = new int[2, 2]
            {
                {0,1},
                {1,0},
            };

            Assert.Equal(1, logic.Calculate(input));
        }

        [Fact]
        public void PerformanceComparisionTest()
        {
            const int LENGTH = 10000;

            var input = new int[LENGTH, LENGTH];
            for (int i = 0; i < LENGTH; i++)
                for (int j = 0; j < LENGTH; j++)
                {
                    input[i,j] = i % 2;
                }



            //+++++++++++++++++++++++++++++++++++++++++++++++++++

            Stopwatch sw = new Stopwatch();

            sw.Start();

            IPatientGroup logic = new GFG();
            Assert.Equal(LENGTH / 2, logic.Calculate(input));

            sw.Stop();

            output.WriteLine($"Elapsed time using {nameof(GFG)}={sw.Elapsed}");

            //+++++++++++++++++++++++++++++++++++++++++++++++++++

            sw = new Stopwatch();

            sw.Start();

            logic = new FastFloodFill();
            Assert.Equal(LENGTH / 2, logic.Calculate(input));

            sw.Stop();

            output.WriteLine($"Elapsed time using {nameof(FastFloodFill)}={sw.Elapsed}");

            //+++++++++++++++++++++++++++++++++++++++++++++++++++


            sw = new Stopwatch();

            sw.Start();

            logic = new PatientGroups();
            Assert.Equal(LENGTH / 2, logic.Calculate(input));

            sw.Stop();

            output.WriteLine($"Elapsed time using {nameof(PatientGroups)}={sw.Elapsed}");
        }

        [Fact]
        public void TestFastFlood1()
        {
            IPatientGroup logic = new FastFloodFill();

            var input = new int[6, 6]
            {
                {1, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 1},
                {1, 1, 0, 1, 0, 0}
            };

            Assert.Equal(4, logic.Calculate(input));
        }

        [Fact]
        public void TestGFG1()
        {
            IPatientGroup logic = new GFG();

            var input = new int[6, 6]
            {
                {1, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 1},
                {1, 1, 0, 1, 0, 0}
            };

            Assert.Equal(4, logic.Calculate(input));
        }
    }
}
