﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalDirectorBusiness
{
    public interface IPatientGroup
    {
        int Calculate(int[,] input);
    }
}
