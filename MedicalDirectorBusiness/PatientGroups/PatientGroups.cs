﻿using System;

namespace MedicalDirectorBusiness
{
    public class PatientGroups : IPatientGroup
    {
        private const int FLAG_CHECKED = 1;
        private const int FLAG_UNCHECKED = 0;
        private const int INPUT_EMPTY = 0;

        public int Calculate(int[,] input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            var groupCount = 0;
            var flags = new int[input.GetLength(0), input.GetLength(1)];

            for (int i = 0; i < input.GetLength(0); i++)
            {
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    if (flags[i, j] == FLAG_UNCHECKED && input[i, j] != INPUT_EMPTY)
                    {
                        groupCount++;

                        SetConnectedFlags(input, flags, i, j);
                    }
                }
            }

            return groupCount;
        }

        private void SetConnectedFlags(int[,] input, int[,] flags, int row, int column)
        {
            flags[row, column] = FLAG_CHECKED;

            for (int i = Math.Max(0, row - 1); i < Math.Min(flags.GetLength(0), row + 2); i++)
            {
                for (int j = Math.Max(0, column - 1); j < Math.Min(flags.GetLength(1), column + 2); j++)
                {
                    if (input[i,j] != INPUT_EMPTY && flags[i,j]==FLAG_UNCHECKED)
                    {
                        SetConnectedFlags(input, flags, i, j);
                    }
                }
            }
        }
    }
}
