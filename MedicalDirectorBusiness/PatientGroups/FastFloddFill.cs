﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedicalDirectorBusiness
{
    public class FastFloodFill : IPatientGroup
    {
        public int Calculate(int[,] input)
        {
            if (input == null)
            {
                return 0;
            }

            //cloning the input so that we don't ruin the input array
            input = input.Clone() as int[,];

            int x = 0;
            int y = 0;
            int numberOfGroups = 0;

            // Move through the matrix, left to right, top to bottom
            while (y < input.GetLength(1) && x < input.GetLength(0))
            {
                while (x < input.GetLength(0))
                {
                    if (input[x,y]!=0)
                    {
                        // Increment if a patient is detected
                        ++numberOfGroups;

                        // Clear all other patients in that group
                        FloodFill(input, (x, y));
                    }
                    ++x;
                }
                x = 0;
                ++y;
            }

            return numberOfGroups;
        }

        /// <summary>
        /// Flood Fill algorithm
        /// Based on: https://www.codeproject.com/Articles/16405/Queue-Linear-Flood-Fill-A-Fast-Flood-Fill-Algorith
        /// </summary>
        /// <param name="patientGroup">Patient group</param>
        /// <param name="startPoint">Start point</param>
        /// <returns>A matrix of patient group</returns>
        private void FloodFill(int[,] patientGroup, (int x, int y) startPoint)
        {
            // Stack of all the points that we need to explore
            Stack<(int x, int y)> stack = new Stack<(int x, int y)>();

            // Add to the starting point
            stack.Push(startPoint);

            while (stack.Count > 0)
            {
                (int x, int y) currentPoint = stack.Pop();

                if (currentPoint.x >= 0 &&
                    currentPoint.y >= 0 &&
                    currentPoint.y < patientGroup.GetLength(1) &&
                    currentPoint.x < patientGroup.GetLength(0))
                {
                    if (patientGroup[currentPoint.x, currentPoint.y] == 1 )
                    {
                        // Perform fill
                        patientGroup[currentPoint.x, currentPoint.y] = 0;

                        // Find all adjacent point co-ordinates
                        var adjacentPoints = Enumerable.Range(-1, 3)
                                  .SelectMany(x => Enumerable.Range(-1, 3).Select(y => (currentPoint.x + x, currentPoint.y + y)))
                                  .Where(((int x, int y) point) => !(point.x == 0 && point.y == 0));

                        foreach ((int, int) point in adjacentPoints)
                        {
                            // Add them into the stack
                            stack.Push(point);
                        }
                    }
                }
            }

        }
    }
}
