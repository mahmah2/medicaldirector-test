﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalDirectorBusiness;
using MedicalDirectorTest.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MedicalDirectorTest.Controllers
{
    [Route("api/patient-groups")]
    [ApiController]
    public class PatientGroupsController : ControllerBase
    {
        private IPatientGroup _patientGroup;

        public PatientGroupsController(IPatientGroup patientGroup)
        {
            _patientGroup = patientGroup;
        }

        [Route("calculate")]
        [ProducesResponseType(typeof(PatientGroupResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Calculate([FromBody] PatientGroupRequest request )
        {
            try
            {
                var result = default(PatientGroupResult);

                await Task.Run(() => {
                    result = new PatientGroupResult(_patientGroup.Calculate(request.matrix));
                });

                return Ok( result );
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
