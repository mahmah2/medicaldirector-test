﻿namespace MedicalDirectorTest.Models
{
    public class PatientGroupResult
    {
        public int NumberOfGroups { get; set; }

        public PatientGroupResult(int numberOfGroups)
        {
            NumberOfGroups = numberOfGroups;
        }
    }
}
